import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StuffComponent } from './stuff/stuff.component';
import { LandingComponent } from './landing/landing.component';


const routes: Routes = [
  { path: 'stuff', component: StuffComponent},
  { path: 'landing', component: LandingComponent},
  { path: '', redirectTo: '/landing', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
