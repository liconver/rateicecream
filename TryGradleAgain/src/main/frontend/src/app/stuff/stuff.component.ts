import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stuff',
  template: `
    <p>
      stuff works!
    </p>
  `,
  styles: []
})
export class StuffComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
