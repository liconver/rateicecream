package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class ForwardController {
	
	@GetMapping(value = {"/landing", "/stuff", "/..."})
	public String frontend() {
		return "forward:/";
	}

}
