package com.bucket;

public enum BucketName {
	
	PROFILE_IMAGE("liam-converse-image-upload-course");
	
	private final String bucketName;

	BucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getBucketName() {
		return bucketName;
	}

}
