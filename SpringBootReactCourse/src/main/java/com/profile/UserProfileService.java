package com.profile;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bucket.BucketName;
import com.filestore.FileStore;

import static org.apache.http.entity.ContentType.*;

@Service
public class UserProfileService {
	
	private final UserProfileDataAccessService userProfileDataAccessService;
	private final FileStore fileStore;
	
	@Autowired
	public UserProfileService(UserProfileDataAccessService userProfileDataAccessService, FileStore fileStore) {
		this.userProfileDataAccessService = userProfileDataAccessService;
		this.fileStore = fileStore;
	}
	
	List<UserProfile> getUserProfiles() {
		return userProfileDataAccessService.getUserProfiles();
	}

	public void uploadUserProfileImage(UUID userProfileId, MultipartFile file) {
		// check if image is not empty, if it is an image, user exists in db,
		// grab some metadata from file if its there, store image in s3
		if (file.isEmpty()) {
			throw new IllegalStateException("cannot upload empty file ["+ file.getContentType()+"] ");
		}
		if (!Arrays.asList(IMAGE_JPEG.getMimeType(), IMAGE_PNG.getMimeType(), IMAGE_GIF.getMimeType()).contains(file.getContentType())) {
			throw new IllegalStateException("file must be an image");
		}
		
		UserProfile user = userProfileDataAccessService.getUserProfiles()
			.stream()
			.filter(userProfile -> userProfile.getUserProfileId().equals(userProfileId))
			.findFirst()
			.orElseThrow(() -> new IllegalStateException(String.format("User profile %s not found", userProfileId)));
		
		Map<String, String> metadata = new HashMap<>();
		metadata.put("Content-Type", file.getContentType());
		metadata.put("Content-Length", String.valueOf(file.getSize()));
		
		String path = String.format("%s/%s", BucketName.PROFILE_IMAGE.getBucketName(), user.getUserProfileId());
		String fileName = String.format("%s-%s", file.getOriginalFilename(), UUID.randomUUID());
		try {
			fileStore.save(path, fileName, Optional.of(metadata), file.getInputStream() );
			user.setUserProfileImageLink(fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new IllegalStateException(e);
		}
	}
	
	

}
